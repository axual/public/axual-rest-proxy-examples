REST_HOST="https://platform.local:18111"

ENVIRONMENT="example"
STREAM="applicationlogevents"

JSON_HEADER="Content-Type: application/json"

echo "Fetching schema IDs"
curl --request POST \
 --url "${REST_HOST}/schema/${ENVIRONMENT}/${STREAM}" \
 --header "$JSON_HEADER" \
 --key ../../../client-cert/helm/security/applications/example-producer/pem/example_producer.key \
 --cert ../../../client-cert/helm/security/applications/example-producer/cer/example_producer.cer \
 --cacert ../../../client-cert/helm/security/applications/common-truststore/cachain/common-truststore.pem \
 --data '{
           "keySchema": "{\"type\":\"record\",\"name\":\"Application\",\"namespace\":\"io.axual.client.example.schema\",\"doc\":\"\",\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"doc\":\"\"},{\"name\":\"version\",\"type\":[\"null\",\"string\"],\"doc\":\"\",\"default\":null},{\"name\":\"owner\",\"type\":[\"null\",\"string\"],\"doc\":\"\",\"default\":null}]}",
           "valueSchema": "{\"type\":\"record\",\"name\":\"ApplicationLogEvent\",\"namespace\":\"io.axual.client.example.schema\",\"doc\":\"\",\"fields\":[{\"name\":\"timestamp\",\"type\":\"long\",\"doc\":\"\"},{\"name\":\"source\",\"type\":{\"type\":\"record\",\"name\":\"Application\",\"doc\":\"\",\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"doc\":\"\"},{\"name\":\"version\",\"type\":[\"null\",\"string\"],\"doc\":\"\",\"default\":null},{\"name\":\"owner\",\"type\":[\"null\",\"string\"],\"doc\":\"\",\"default\":null}]},\"doc\":\"\"},{\"name\":\"context\",\"type\":{\"type\":\"map\",\"values\":\"string\"},\"doc\":\"\"},{\"name\":\"level\",\"type\":{\"type\":\"enum\",\"name\":\"ApplicationLogLevel\",\"doc\":\"\",\"symbols\":[\"DEBUG\",\"INFO\",\"WARN\",\"ERROR\",\"FATAL\"]},\"doc\":\"\"},{\"name\":\"message\",\"type\":\"string\",\"doc\":\"\"}]}"
         }' \
 -s | jq
