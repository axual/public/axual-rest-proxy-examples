REST_HOST="https://localhost:18111"
ENVIRONMENT="example"
STREAM="applicationlogevents"

JSON_HEADER="Content-Type: application/json"

echo "Fetching Schema IDs"
curl -k --request POST \
 --url "${REST_HOST}/schema/${ENVIRONMENT}/${STREAM}" \
 --header "$JSON_HEADER" \
 --key ../../../client-cert/standalone/standalone.key \
 --cert ../../../client-cert/standalone/standalone.cer \
 --cacert ../../../client-cert/standalone/standalone_cacert.cer \
 --data '{
           "keySchema": "{\"type\":\"record\",\"name\":\"Application\",\"namespace\":\"io.axual.client.example.schema\",\"doc\":\"Identification of an application\",\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"doc\":\"The name of the application\"},{\"name\":\"version\",\"type\":[\"null\",\"string\"],\"doc\":\"(Optional) The application version\",\"default\":null},{\"name\":\"owner\",\"type\":[\"null\",\"string\"],\"doc\":\"The owner of the application\",\"default\":null}]}",
           "valueSchema": "{\"type\":\"record\",\"name\":\"ApplicationLogEvent\",\"namespace\":\"io.axual.client.example.schema\",\"doc\":\"Generic application log event\",\"fields\":[{\"name\":\"timestamp\",\"type\":\"long\",\"doc\":\"Timestamp of the event\"},{\"name\":\"source\",\"type\":{\"type\":\"record\",\"name\":\"Application\",\"doc\":\"Identification of an application\",\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"doc\":\"The name of the application\"},{\"name\":\"version\",\"type\":[\"null\",\"string\"],\"doc\":\"(Optional) The application version\",\"default\":null},{\"name\":\"owner\",\"type\":[\"null\",\"string\"],\"doc\":\"The owner of the application\",\"default\":null}]},\"doc\":\"The application that sent the event\"},{\"name\":\"context\",\"type\":{\"type\":\"map\",\"values\":\"string\"},\"doc\":\"The application context, contains application-specific key-value pairs\"},{\"name\":\"level\",\"type\":{\"type\":\"enum\",\"name\":\"ApplicationLogLevel\",\"doc\":\"The level of the log message\",\"symbols\":[\"DEBUG\",\"INFO\",\"WARN\",\"ERROR\",\"FATAL\"]},\"doc\":\"The log level, being either DEBUG, INFO, WARN or ERROR\"},{\"name\":\"message\",\"type\":\"string\",\"doc\":\"The log message\"}]}"
         }' \
  -s | jq

echo
