REST_HOST="https://localhost:18111"
ENVIRONMENT="example"
STREAM="applicationlogevents"
UUID="producer1"
APP_ID="io.axual.example.client.avro.producer"

JSON_HEADER="Content-Type: application/json"

# Before producing message, try to get the key and value schemaId by executing 'rest-proxy-standalone-avro-schema.sh' file.
# Replace keyId and valueId as keyMessage schemaId and valueMessage schemaId.
PRODUCE_RECORD='
{
   "keyMessage":{
      "type":"AVRO",
      "schemaId": 719967681,
      "message":"{\"name\": \"logeventproducer\", \"version\": \"0.0.1\", \"owner\": \"none\"}"
   },
   "valueMessage":{
      "type":"AVRO",
      "schemaId": 358926375,
      "message":"{\"timestamp\": 1009, \"source\": {\"name\": \"logeventproducer\", \"version\": \"0.0.1\", \"owner\": \"Team Log\"}, \"context\": {\"Some key\": \"Some Value\"}, \"level\": \"INFO\", \"message\": \"Hello Axual\"}"
   }
}'

echo "Sending"
curl -k --request POST \
  --url "${REST_HOST}/stream/${ENVIRONMENT}/${STREAM}" \
  --header "axual-application-id: $APP_ID" \
  --header 'axual-application-version: 1.0' \
  --header "axual-producer-uuid: $UUID" \
  --header "$JSON_HEADER" \
  --key ../../../client-cert/standalone/standalone.key \
  --cert ../../../client-cert/standalone/standalone.cer \
  --cacert ../../../client-cert/standalone/standalone_cacert.cer \
  --data "$PRODUCE_RECORD" \
  -s | jq
