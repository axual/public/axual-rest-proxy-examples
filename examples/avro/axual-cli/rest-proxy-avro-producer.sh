REST_HOST="https://192.168.99.100:18100"
ENVIRONMENT="example"
STREAM="applicationlogevents"
UUID="producer1"
APP_ID="io.axual.example.client.avro.producer"

JSON_HEADER="Content-Type: application/json"

# Before producing message, try to get the key and value schemaId by executing 'rest-proxy-local-avro-schema.sh' file.
# Replace keyId and valueId as keyMessage schemaId and valueMessage schemaId.
PRODUCE_RECORD='
{
   "keyMessage":{
      "type":"AVRO",
      "schemaId": 1,
      "message":"{\"name\": \"logeventproducer\", \"version\": \"0.0.1\", \"owner\": \"none\"}"
   },
   "valueMessage":{
      "type":"AVRO",
      "schemaId": 2,
      "message":"{\"timestamp\": 1009, \"source\": {\"name\": \"logeventproducer\", \"version\": \"0.0.1\", \"owner\": \"Team Log\"}, \"context\": {\"Some key\": \"Some Value\"}, \"level\": \"INFO\", \"message\": \"Message 9\"}"
   }
}'

echo "Sending"
curl --request POST \
  --url "${REST_HOST}/stream/${ENVIRONMENT}/${STREAM}" \
  --header "axual-application-id: $APP_ID" \
  --header 'axual-application-version: 1.0' \
  --header "axual-producer-uuid: $UUID" \
  --header "$JSON_HEADER" \
  --key ../../../client-cert/axual-cli/security/applications/example-producer/pem/example_producer.key \
  --cert ../../../client-cert/axual-cli/security/applications/example-producer/cer/example_producer.cer \
  --cacert ../../../client-cert/axual-cli/security/applications/common-truststore/cachain/common-truststore.pem \
  --data "$PRODUCE_RECORD" \
  -s | jq
