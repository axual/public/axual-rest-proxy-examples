REST_HOST="https://192.168.99.100:18100"
ENVIRONMENT="example"
STREAM="applicationlogevents"

JSON_HEADER="Content-Type: application/json"


echo "Receiving"
curl --request POST \
 --url "${REST_HOST}/schema/${ENVIRONMENT}/${STREAM}" \
 --header "$JSON_HEADER" \
 --key ../../../client-cert/axual-cli/security/applications/example-producer/pem/example_producer.key \
 --cert ../../../client-cert/axual-cli/security/applications/example-producer/cer/example_producer.cer \
 --cacert ../../../client-cert/axual-cli/security/applications/common-truststore/cachain/common-truststore.pem \
 --data '{
           "keySchema": "{\"type\":\"record\",\"name\":\"Application\",\"namespace\":\"io.axual.client.example.schema\",\"doc\":\"Identification of an application\",\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"doc\":\"The name of the application\"},{\"name\":\"version\",\"type\":[\"null\",\"string\"],\"doc\":\"(Optional) The application version\",\"default\":null},{\"name\":\"owner\",\"type\":[\"null\",\"string\"],\"doc\":\"The owner of the application\",\"default\":null}]}",
           "valueSchema": "{\"type\":\"record\",\"name\":\"ApplicationLogEvent\",\"namespace\":\"io.axual.client.example.schema\",\"doc\":\"Generic application log event\",\"fields\":[{\"name\":\"timestamp\",\"type\":\"long\",\"doc\":\"Timestamp of the event\"},{\"name\":\"source\",\"type\":{\"type\":\"record\",\"name\":\"Application\",\"doc\":\"Identification of an application\",\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"doc\":\"The name of the application\"},{\"name\":\"version\",\"type\":[\"null\",\"string\"],\"doc\":\"(Optional) The application version\",\"default\":null},{\"name\":\"owner\",\"type\":[\"null\",\"string\"],\"doc\":\"The owner of the application\",\"default\":null}]},\"doc\":\"The application that sent the event\"},{\"name\":\"context\",\"type\":{\"type\":\"map\",\"values\":\"string\"},\"doc\":\"The application context, contains application-specific key-value pairs\"},{\"name\":\"level\",\"type\":{\"type\":\"enum\",\"name\":\"ApplicationLogLevel\",\"doc\":\"The level of the log message\",\"symbols\":[\"DEBUG\",\"INFO\",\"WARN\",\"ERROR\",\"FATAL\"]},\"doc\":\"The log level, being either DEBUG, INFO, WARN or ERROR\"},{\"name\":\"message\",\"type\":\"string\",\"doc\":\"The log message\"}]}"
         }'
 -s | jq