#!/bin/bash
REST_HOST="https://localhost:18111"
ENVIRONMENT="example"
STREAM="applicationlogevents-string"
UUID="producer1"
APP_ID="io.axual.example.client.string.producer"

JSON_HEADER="Content-Type: application/json"

PRODUCE_RECORD='{
   "keyMessage":{
      "type":"STRING",
      "message":"Random key: '$RANDOM'"
   },
   "valueMessage":{
      "type":"STRING",
      "message":"Random value: '$RANDOM'"
   }
}'

echo "Sending"
curl -k --request POST \
  --url "${REST_HOST}/stream/${ENVIRONMENT}/${STREAM}" \
  --header "axual-application-id: $APP_ID" \
  --header 'axual-application-version: 1.0' \
  --header "axual-producer-uuid: $UUID" \
  --header "$JSON_HEADER" \
  --key ../../../client-cert/standalone/standalone.key \
  --cert ../../../client-cert/standalone/standalone.cer \
  --cacert ../../../client-cert/standalone/standalone_cacert.cer \
  --data "$PRODUCE_RECORD" \
  -s | jq
