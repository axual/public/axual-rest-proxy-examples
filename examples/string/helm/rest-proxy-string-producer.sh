#!/bin/bash
REST_HOST="https://platform.local:18111"
ENVIRONMENT="example"
STREAM="applicationlogevents-string"
UUID="producer1"
APP_ID="io.axual.example.client.string.producer"

JSON_HEADER="Content-Type: application/json"

PRODUCE_RECORD='{
   "keyMessage":{
      "type":"STRING",
      "message":"Random key: '$RANDOM'"
   },
   "valueMessage":{
      "type":"STRING",
      "message":"Random value: '$RANDOM'"
   }
}'

echo "Sending"
curl -k --request POST \
  --url "${REST_HOST}/stream/${ENVIRONMENT}/${STREAM}" \
  --header "axual-application-id: $APP_ID" \
  --header 'axual-application-version: 1.0' \
  --header "axual-producer-uuid: $UUID" \
  --header "$JSON_HEADER" \
  --key ../../../client-cert/helm/security/applications/example-producer/pem/example_producer.key \
  --cert ../../../client-cert/helm/security/applications/example-producer/cer/example_producer.cer \
  --cacert ../../../client-cert/helm/security/applications/common-truststore/cachain/common-truststore.pem \
  --data "$PRODUCE_RECORD" \
  -s | jq
