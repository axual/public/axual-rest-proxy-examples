#!/bin/bash
REST_HOST="https://platform.local:18111"
ENVIRONMENT="example"
STREAM="applicationincidents-json"
UUID="analyzer1"
APP_ID="io.axual.example.client.json.consumer"

JSON_HEADER="Content-Type: application/json"

LOOP="false"
if [[ $# -gt 0 ]] && [[ "$1" =~ l|Lo|Oo|Op|P ]]; then
      LOOP="true"
      echo "Loop enabled, press CTRL-C to quit"
fi

function consume_messages {
  echo "Receiving"
  curl -k --request GET \
    --url "${REST_HOST}/stream/${ENVIRONMENT}/${STREAM}" \
    --header "axual-application-id: $APP_ID" \
    --header 'axual-application-version: 1.0' \
    --header "axual-consumer-uuid: $UUID" \
    --header 'axual-key-type: STRING' \
    --header 'axual-value-type: JSON' \
    --header 'axual-commit-strategy: AFTER_READ' \
    --header 'axual-polling-timeout-ms: 10000' \
    --header "$JSON_HEADER" \
    --key ../../../client-cert/helm/security/applications/example-consumer/pem/example_consumer.key \
    --cert ../../../client-cert/helm/security/applications/example-consumer/cer/example_consumer.cer \
    --cacert ../../../client-cert/helm/security/applications/common-truststore/cachain/common-truststore.pem \
    -s | jq
}

echo

if [[ "$LOOP" == "true" ]];then
  while [[ "0" == "0" ]]; do
    consume_messages
    echo "Looping, press CTRL-C to quit"
    sleep 2
  done
else
  consume_messages
fi
