#!/bin/bash
REST_HOST="https://localhost:18111"
ENVIRONMENT="example"
STREAM="applicationincidents-json"
UUID="producer1"
APP_ID="io.axual.example.client.json.producer"

JSON_HEADER="Content-Type: application/json"

PRODUCE_TEMPLATE='
{
  "keyMessage": {
    "type": "STRING",
    "message": ""
  },
  "valueMessage": {
    "type": "JSON",
    "message": {
      "incidentId": "",
      "userId": "U-2019-12-24-000003",
      "userData": {
        "firstName": "Rocky",
        "lastName": "Balboa",
        "birthDay": "1981-10-08"
      },
      "severity": "",
      "reportDate": "2020-03-28",
      "reportText": "The system was fully unresponsive"
    }
  }
}
'

INCIDENT_ID=$RANDOM
REPORT_DATE="$(date +"%Y-%m-%d")"

SEVERITY=UNKNOWN

MOD=$((${RANDOM}%4))
case $MOD in
 0) SEVERITY=LOW;;
 1) SEVERITY=MEDIUM;;
 2) SEVERITY=HIGH;;
 3) SEVERITY=CRITICAL;;
esac

PAYLOAD=$(echo "$PRODUCE_TEMPLATE" | jq --arg sev "$SEVERITY" --arg iid "$INCIDENT_ID" --arg reportDate "$REPORT_DATE" '.keyMessage.message = $iid | .valueMessage.message.incidentId = $iid | .valueMessage.message.reportDate = $reportDate | .valueMessage.message.severity = $sev'  )

echo
echo "Payload:"
echo
echo "$PAYLOAD" | jq


echo "Sending"
curl -k --request POST \
  --url "${REST_HOST}/stream/${ENVIRONMENT}/${STREAM}" \
  --header "axual-application-id: $APP_ID" \
  --header 'axual-application-version: 1.0' \
  --header "axual-producer-uuid: $UUID" \
  --header "$JSON_HEADER" \
  --key ../../client-cert/standalone/standalone.key \
  --cert ../../client-cert/standalone/standalone.cer \
  --cacert ../../client-cert/standalone/standalone_cacert.cer \
  --data "$PAYLOAD" \
  -s | jq

