#!/bin/bash
REST_HOST="https://localhost:18111"
ENVIRONMENT="example"
STREAM="applicationincidents-json"
UUID="analyzer1"
APP_ID="io.axual.example.client.json.consumer"

JSON_HEADER="Content-Type: application/json"

LOOP="false"
if [[ $# -gt 0 ]] && [[ "$1" =~ l|Lo|Oo|Op|P ]]; then
      LOOP="true"
      echo "Loop enabled, press CTRL-C to quit"
fi

function consume_messages {
  echo "Receiving"
  curl -k --request GET \
    --url "${REST_HOST}/stream/${ENVIRONMENT}/${STREAM}" \
    --header "axual-application-id: $APP_ID" \
    --header 'axual-application-version: 1.0' \
    --header "axual-consumer-uuid: $UUID" \
    --header 'axual-key-type: STRING' \
    --header 'axual-value-type: JSON' \
    --header 'axual-commit-strategy: AFTER_READ' \
    --header 'axual-polling-timeout-ms: 10000' \
    --header "$JSON_HEADER" \
    --key ../../client-cert/standalone/standalone.key \
    --cert ../../client-cert/standalone/standalone.cer \
    --cacert ../../client-cert/standalone/standalone_cacert.cer \
    -s | jq
}

echo

if [[ "$LOOP" == "true" ]];then
  while [[ "0" == "0" ]]; do
    consume_messages
    echo "Looping, press CTRL-C to quit"
    sleep 2
  done
else
  consume_messages
fi
