# Rest Proxy Examples

This example repo shows a typical use of the Rest Proxy, with consume/produce examples for both streams of type `STRING`/`STRING` (K/V) as `AVRO`/`AVRO`. 

## Run configurations

The examples can be executed in various configurations. For the sake of simplicity, the example focuses on a *standalone* deployment of the platform.

| Configuration | Description | Default REST Proxy endpoint | Examples |
| --- | --- | --- | --- |
| standalone (preferred) | Axual running locally in 1 single docker container, REST Proxy in another, started together with `docker-compose` up | `https://localhost:18111` | [`STRING`](examples/string/standalone) [`JSON`](examples/json/standalone) [`AVRO`](examples/avro/standalone) |
| helm charts | Axual started in any kubernetes environment using helm charts, see also https://docs.cloud.axual.io/operations/2020.3/deployment/axual-helm.html | `https://platform.local:18111` | [`STRING`](examples/string/helm) [`JSON`](examples/json/helm) [`AVRO`](examples/avro/helm) |
| Axual CLI (deprecated) | Axual started in a local/VM docker environment using the Axual CLI, see also https://docs.cloud.axual.io/operations/2020.3/deployment/axual-cli.html | `https://192.168.99.100:18100` | [`STRING`](examples/string/axual-cli) [`JSON`](examples/json/axual-cli) [`AVRO`](examples/avro/axual-cli) |

## Getting started

The instructions below assume you will start the platform locally in **standalone** mode. For other starting instructions, refer to the table above. 

The scripts will produce to and consume from the `applicationlogevents` stream and use the `Application`/`ApplicationLogEvent` schemas (AVRO examples only). 

To get started, follow along with the AVRO example below:

> Prerequisite: you have `curl` and `jq` installed locally on your machine

1. First, start the platform locally:
    ```bash
    docker-compose up
    ```
    TIP: use flag `-d` to start the process in the background
2. Fetch the schema IDs to be used in the *produce* call:  
   
    ```bash
    cd examples/avro/standalone
    ./rest-proxy-avro-schema.sh
    
    Fetching Schema IDs
    {"keyId":719967681,"valueId":358926375}
    ```
    > The schema ID for Key and Value will be always the same when using the platform in standalone mode. In a real, non-local installation, always retrieve schemas before you make a produce call for any stream.
    
3. Run a script to produce a single random message locally. The *Key* and *Value* schema IDs are referred to in the `rest-proxy-avro-producer.sh` script

    ```bash
    ./rest-proxy-avro-producer.sh
    
     Sending
     {"cluster":"clusterA","offset":2,"timestamp":1612790414831,"stream":"applicationlogevents","partition":5}
    ```
4. Run a script to consume the message you have just produced
    ```bash
    ./rest-proxy-avro-consumer.sh
    
    Receiving
    {"cluster":"clusterA","messages":[{"messageId":"710bb28d-bd20-4fe0-beec-95c11fd9e2d8","produceTimestamp":1612790563664,"partition":0,"offset":4,"produceCluster":"clusterA","consumeCluster":"clusterA","headers":{"Axual-Producer-Version":["MS4w"],"Axual-Copy-Flags":["AAAAAA=="],"Axual-Message-Id":["cQuyjb0gT+C+7JXBH9ni2A=="],"Axual-Tenant":["cGxhdGZvcm0tdGVzdC1zdGFuZGFsb25l"],"Axual-Serialization-Time":["AAABd4HPA1A="],"Axual-Environment":["bG9jYWw="],"Axual-Deserialization-Time":["AAABd4HPEXk="],"Axual-Intermediate-Version":["MS4w"],"Axual-System":["cGxhdGZvcm0tdGVzdC1zdGFuZGFsb25l"],"Axual-Intermediate-Id":["aW8uYXh1YWwuZXhhbXBsZS5jbGllbnQuYXZyby5jb25zdW1lcg=="],"Axual-Cluster":["Y2x1c3RlckE="],"Axual-Instance":["bG9jYWw="],"Axual-Producer-Id":["aW8uYXh1YWwuZXhhbXBsZS5jbGllbnQuYXZyby5wcm9kdWNlcg=="]},"keyMessage":{"type":"AVRO","schema":null,"schemaId":null,"message":"{\"name\": \"logeventproducer\", \"version\": \"0.0.1\", \"owner\": \"none\"}"},"valueMessage":{"type":"AVRO","schema":null,"schemaId":null,"message":"{\"timestamp\": 1009, \"source\": {\"name\": \"logeventproducer\", \"version\": \"0.0.1\", \"owner\": \"Team Log\"}, \"context\": {\"Some key\": \"Some Value\"}, \"level\": \"INFO\", \"message\": \"Hello Axual\"}"}}]}
    ``` 
5. Want to find out more? Refer to the [`rest-proxy-avro-schema.sh`](examples/avro/standalone/rest-proxy-avro-schema.sh), [`rest-proxy-avro-producer.sh`](examples/avro/standalone/rest-proxy-avro-producer.sh) and [`rest-proxy-avro-consumer.sh`](examples/avro/standalone/rest-proxy-avro-consumer.sh) scripts to see how the interaction with REST Proxy works. Or, try one of the `JSON` or `STRING` examples.

## Reference documentation

Please refer to our [online documentation](https://docs.cloud.axual.io/rest-proxy/1.2.2/index.html) for more detailed service documentation.

## License

Axual Rest Proxy Examples is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).